package com.celfocus.noccias.kafka

import java.util.Properties

import com.celfocus.noccias.avro.AvroDecoder
import kafka.consumer.Whitelist
import kafka.serializer.DefaultDecoder
import org.apache.avro.io.Decoder
import org.apache.kafka.clients.consumer.{Consumer, ConsumerConfig}

class Consumer(decoder: AvroDecoder, topic: String) {

  private val props = new Properties() //VER ISTO - FAZER CONTROL Z até ter a zookeeper connection para o porto 2181 e pedir ao joao para explicar como aceder ao porto pela minha maquina
  val groupId = "kafka_Avro_Reader"
  props.put("group.id", groupId)
  props.put("bootstrap.servers", "cia-sit-cog-brk1:9092")
  props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
  props.put("auto.offset.reset", "largest")
  props.put("consumer.timeout.ms", "120000")
  props.put("auto.commit.interval.ms", "10")
  props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")

  private val consumerConfig = new ConsumerConfig(props)
  private val consumerConnector = Consumer.create(consumerConfig)
  private val filterSpec = new Whitelist(topic)
  private val streams = consumerConnector.createMessageStreamsByFilter(filterSpec, 1, new DefaultDecoder(), new DefaultDecoder())(0)
  lazy val iterator = streams.iterator()

  def read() =
    try {
      if (hasNext) {
        println("Getting message from queue.............")
        val message: Array[Byte] = iterator.next().message()
        println(decoder.getObject(message))
      } else {
        None
      }
    } catch {
      case ex: Exception => ex.printStackTrace()
        None
    }

  private def hasNext: Boolean =
    try
      iterator.hasNext()
    catch {
      case timeOutEx: ConsumerTimeoutException =>
        false
      case ex: Exception => ex.printStackTrace()
        println("Got error when reading message ")
        false
    }
}
