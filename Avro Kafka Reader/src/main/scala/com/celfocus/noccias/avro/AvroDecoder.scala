package com.celfocus.noccias.avro

import org.apache.avro.Schema
import org.apache.avro.generic.GenericRecord
import org.apache.avro.io.{DatumReader, Decoder, DecoderFactory}
import org.apache.avro.specific.SpecificDatumReader

import scala.io.Source

class AvroDecoder(schemaPath: String) {

  //read avro schema file
  val schemaString = Source.fromFile(schemaPath).mkString
  // Initialize schema
  val schema: Schema = new Schema.Parser().parse(schemaString)

  //(ver)
  def getObject(message: Array[Byte]) = {
    // Deserialize and create generic record
    val reader: DatumReader[GenericRecord] = new SpecificDatumReader[GenericRecord](schema)
    val decoder: Decoder = DecoderFactory.get().binaryDecoder(message, null)
    val data: GenericRecord = reader.read(null, decoder)
    val dataStr = data.toString

    dataStr
  }
}
