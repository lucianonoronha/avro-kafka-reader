package com.celfocus.noccias

import com.celfocus.noccias.avro.{AvroDecoder, Encoder}
import com.celfocus.noccias.kafka.{Consumer, Producer}

object App {

  def main(args: Array[String]): Unit = {

    //val schemaPath = args[0]
    //val topic = args[1]
    val topic = "PoC_A4E_EAS_Alarms"
    val schemaPath = """D:\IdeaProjects\scripts\Full\4.HDFS\AvroSchemas\alarm.avsc"""
    println("passo 1")
    val encoder = new Encoder(schemaPath)
    println("passo 2")
    val decoder = new AvroDecoder(schemaPath)
    println("passo 3")

    val kafkaProducer = new Producer(encoder)
    println("passo 4")
    val kafkaConsumer = new Consumer(decoder, topic)
    println("passo 5")
    kafkaConsumer.read()
    println("acabou")
  }
}
